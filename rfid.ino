/**
 * ----------------------------------------------------------------------------
 * This is a MFRC522 library example; see https://github.com/miguelbalboa/rfid
 * for further details and other examples.
 *
 * NOTE: The library file MFRC522.h has a lot of useful info. Please read it.
 *
 * Released into the public domain.
 * ----------------------------------------------------------------------------
 * This sample shows how to read and write data blocks on a MIFARE Classic PICC
 * (= card/tag).
 *
 * BEWARE: Data will be written to the PICC, in sector #1 (blocks #4 to #7).
 *
 *
 * Typical pin layout used:
 * -----------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
 *             Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin
 * -----------------------------------------------------------------------------------------
 * RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
 * SPI SS      SDA(SS)      10            53        D10        10               10
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
 *
 */

#include <SPI.h>
#include <MFRC522.h>
#include <string>
#include <WiFi.h>

#define RST_PIN         22           // Configurable, see typical pin layout above
#define SS_PIN          21          // Configurable, see typical pin layout above
#define PIN_BUZZER      32
const char* ssid     = "Tenda";
const char* password = "asdf1234";
// const char* host = "192.168.8.134";
// const int httpPort = 4000;
const char* host = "123.108.10.200";
const char* host2 = "192.168.8.134";
const int httpPort = 80;
const uint8_t pinInput = 35;

int last = 0;
int count = 1;

byte sector         = 1;
byte blockAddr      = 4;

byte trailerBlock   = 7;

byte buffer[18];
byte size = sizeof(buffer);

MFRC522::StatusCode status;
MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

MFRC522::MIFARE_Key key;

/**
 * Initialize.
 */

void setupwifi(){
    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());  
} 

void setuprfid(){
   while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
    SPI.begin();        // Init SPI bus
    mfrc522.PCD_Init(); // Init MFRC522 card
}

void setup() {
    Serial.begin(115200); // Initialize serial communications with the PC
    setupwifi();
    setuprfid();
    pinMode(PIN_BUZZER, OUTPUT);
    pinMode(pinInput,INPUT); //Pin 5 as signal input

    delay(10);

   
}

void HandleSensor(){
    if(digitalRead(pinInput) == 0)
    {
        if(digitalRead(pinInput) != last)
        {
            Serial.print("connecting to ");
            Serial.println(host);
        
            // Use WiFiClient class to create TCP connections
            WiFiClient client;
            const int httpPort2 = 4000;
            if (!client.connect(host2, httpPort2)) {
                Serial.println("connection failed");
                return;
            }
        
            // We now create a URI for the request
            String url = "/po/count/";
            url += String(count++);
        
            Serial.print("Requesting URL: ");
            Serial.println(url);
        
            // This will send the request to the server
            client.print(String("GET ") + url + " HTTP/1.1\r\n" +
                        "Host: " + host2 + "\r\n" +
                        "Connection: close\r\n\r\n");
            unsigned long timeout = millis();
            while (client.available() == 0) {
                if (millis() - timeout > 5000) {
                    Serial.println(">>> Client Timeout !");
                    client.stop();
                    return;
                }
            }
        
            // Read all the lines of the reply from server and print them to Serial
            while(client.available()) {
                String line = client.readStringUntil('\r');
                Serial.print(line);
            }
        
            Serial.println();
            Serial.println("closing connection");
        }
    } 

  last = digitalRead(pinInput);
}

void handleWifi(String uid,std::string hasilfix){
     Serial.print("connecting to ");
    Serial.print(host);
    Serial.print(":");
    Serial.print(httpPort);
  
    // Use WiFiClient class to create TCP connections
    WiFiClient client;
    if (!client.connect(host, httpPort)) {
        Serial.println("connection failed");
        return;
    }

    // rudi
   String url = "/gw-eticket/gw-eticket.php?uid=";
   url += uid;
   url +="&id=bis1&saldo=";
   url +=hasilfix.c_str();
   url +="&harga=25000";

    //my dummy
    // String url="/po/emoney/";
    // url+=uid;
    // url+="/";
    // url+=hasilfix.c_str();
    // url+="/25000";

    Serial.print("Requesting URL: ");
    Serial.println(url);

    // This will send the request to the server
    client.print(String("GET ") + url + " HTTP/1.1\r\n" +
                 "Host: " + host + "\r\n" +
                 "Connection: close\r\n\r\n");
    unsigned long timeout = millis();
    while (client.available() == 0) {
        if (millis() - timeout > 5000) {
            Serial.println(">>> Client Timeout !");
            client.stop();
            return;
        }
    }
    String xxx2;
    String xxx;
  
    // Read all the lines of the reply from server and print them to Serial
    uint8_t aa = 0;
    while(client.available()) {
        xxx2 = client.readStringUntil('{');
        if(aa==1)
            xxx = xxx2;
            Serial.print("line - ");
            Serial.print(xxx);
        aa++;
    }

    Serial.print("saldo = ");
    Serial.print(xxx);
 
    byte dataBlock2[16];
    char str_array[18];
    xxx.toCharArray(str_array,18);
    for(int i = 0;i<16;i++){
        dataBlock2[i] = str_array[i];
        Serial.println(str_array[i],HEX);
    }
     // Authenticate using key B
    Serial.println(F("Authenticating again using key B..."));
    status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_B, trailerBlock, &key, &(mfrc522.uid));
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("PCD_Authenticate() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
        return;
    }

    // Write data to the block
    Serial.print(F("Writing data into block ")); Serial.print(blockAddr);
    Serial.println(F(" ..."));
    //dump_byte_array(dataBlock, 16); Serial.println();
    status = (MFRC522::StatusCode) mfrc522.MIFARE_Write(blockAddr, dataBlock2, 16);
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("MIFARE_Write() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
    }
    Serial.println();

    //Serial.printNumber(i);
    Serial.println("closing connection"); 

    mfrc522.PICC_HaltA();
    // Stop encryption on PCD
    mfrc522.PCD_StopCrypto1();  
    digitalWrite(PIN_BUZZER, LOW);   //Buzzer menyala
    delay(500);                       // wait for a second
    digitalWrite(PIN_BUZZER, HIGH);

}

/**
 * Main loop.
 */
void loop() {
    //sensor ir start
   // HandleSensor();
    //sensor ir end

// Prepare the key (used both as key A and as key B)
// using FFFFFFFFFFFFh which is the default at chip delivery from the factory
    for (byte i = 0; i < 6; i++) {
        key.keyByte[i] = 0xFF;
    }

    if ( ! mfrc522.PICC_IsNewCardPresent())
       // Serial.print("tidak ditemukan kartu");
        return;

    // Select one of the cards
    if ( ! mfrc522.PICC_ReadCardSerial())
        return;
        
    Serial.println(F("**Card Detected:**"));

    // Show some details of the PICC (that is: the tag/card)
    Serial.print(F("Card UID:"));
    //dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
    char uidbuffer[10];
    sprintf(uidbuffer, "%X%X%X%X", mfrc522.uid.uidByte[0],mfrc522.uid.uidByte[1],mfrc522.uid.uidByte[2],mfrc522.uid.uidByte[3]);
    String uid = uidbuffer;
    
    Serial.print(F("PICC type: "));
    MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    Serial.println(mfrc522.PICC_GetTypeName(piccType));

   

    // digitalWrite(PIN_BUZZER, HIGH);   //Buzzer menyala
    // delay(500);                       // wait for a second
    // digitalWrite(PIN_BUZZER, LOW);

     // Authenticate using key A
    Serial.println(F("Authenticating using key A..."));
    status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("PCD_Authenticate() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
        return;
    }

    // Read data from the block (again, should now be what we have written)
    Serial.print(F("Reading data from block ")); Serial.print(blockAddr);
    Serial.println(F(" ..."));
    status = (MFRC522::StatusCode) mfrc522.MIFARE_Read(blockAddr, buffer, &size);
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("MIFARE_Read() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
    }
    
    char isi[16];
    sprintf(isi, "%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c",buffer[0],buffer[1],buffer[2],buffer[3],buffer[4],buffer[5],buffer[6],buffer[7],buffer[8],buffer[9],buffer[10],buffer[11],buffer[12],buffer[13],buffer[14],buffer[15]);
    std::string hasil(isi);
    std::string hasilfix = hasil.erase(0, hasil.find_first_not_of('0'));

    handleWifi(uid,hasilfix);
   

}

